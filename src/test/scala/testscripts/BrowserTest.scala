package testscripts

import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class BrowserTest extends Simulation {
	
	val httpProtocolNormalUser = http
		.baseURL("https://tk-eu-dev-tks.mindcurv.com")

	val user = csv("user.csv").circular
	val searchTerm = csv("search.csv").circular
	val products = csv("productIds.csv").circular
	val categoryUrl = csv("main_category.csv").random


	val scenario1= scenario("Scenario 1 - Add to Cart").feed(user).feed(searchTerm).feed(products).group("HomePage"){ exec(HomePage.homePage)
	}.group("LoginPage"){
	exec(LoginPage.userLoginPage)
	}.group("Perform Successfull Login"){
	exec(LogIn.userLogIn)
	}.group("Search For Product"){
	exec(Search.searchProduct)
	}.group("Navigate to pdp"){
	exec(UserPDP.userPDP)
	}.group("Add To Cart"){
	exec(UserCart.addToCart)
	}.group("Navigate To Cart"){
	exec(UserCart.cartPage)
	}.group("Logout from the site"){
	exec(LogOut.userLogOut)
	}

	val scenario2= scenario("Scenario 2 - Browse").feed(user).feed(categoryUrl)
	.group("HomePage"){
	exec(HomePage.homePage)
	}.group("LoginPage"){
	exec(LoginPage.userLoginPage)
	}.group("Perform Successfull Login"){
	exec(LogIn.userLogIn)
	}.group("Main Category Page"){
	exec(MainCategory.mainCategoryPage)
	}.group("Product Details Page"){
	exec(UserPDP.userPDP)
	}.group("Profile Details Page"){
	exec(ProfileDetails.profileDetailsPage)
	}.group("Logout from the site"){
	exec(LogOut.userLogOut)
	}

        setUp(scenario1.inject(atOnceUsers(1)),scenario2.inject(atOnceUsers(1))
).protocols(httpProtocolNormalUser)

}
