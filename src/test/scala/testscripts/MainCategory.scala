package testscripts
import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

object MainCategory {

		val mainCategoryPage = exec(http("MainCategory")
					.get("${mainCategory}")
					.headers(Headers.mainCategoryHeader)
					.check(css("a.thumb", "href").saveAs("pdpUrl")))
}
