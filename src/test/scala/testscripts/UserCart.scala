package testscripts
import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._


object UserCart {

 	val addToCart = exec(http("Add Product To Cart")
			.post("/tkSchulteSite/de/cart/add")
			.headers(Headers.cartPageHeaders_0)
			.formParam("qty", "${quantity}")
			.formParam("productCodePost", "${productId}")
			.formParam("CSRFToken", "${csrf}"))
		
 	val cartPage = exec(http("Cart Page")
			.get("/tkSchulteSite/de/cart")
			.headers(Headers.cartPageHeaders_1))


}
