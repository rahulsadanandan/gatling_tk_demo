package testscripts
import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._



object LoginPage {



	val userLoginPage = exec(http("Login Page")
			.get("/tkSchulteSite/en/login")
			.headers(Headers.userLoginHeaders_0)
			.check(regex("""<input type="hidden" name="CSRFToken" value="(.*?)" />""").saveAs("csrf")))
}

object LogIn {
            val userLogIn = exec(http("Login")
			.post("/tkSchulteSite/en/j_spring_security_check")
			.headers(Headers.userLoginHeaders_1)
			.formParam("j_username", "${username}")
			.formParam("j_password", "${password}")
			.formParam("CSRFToken", "${csrf}"))
}



object LogOut {
            val userLogOut = exec(http("Logout")
			.get("/tkSchulteSite/de/logout")
			.headers(Headers.userLoginHeaders_2))
}
