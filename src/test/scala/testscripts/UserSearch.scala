package testscripts
import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._


object Search {

 	val searchProduct = exec(http("SearchProduct AutoComplete Call")
			.get("/tkSchulteSite/de/search/autocomplete/SearchBox?term=${searchTerm}")
			.headers(Headers.searchPageHeaders_0))

		.exec(http("SearchProduct Call")
			.get("/tkSchulteSite/de/search/?text=${searchTerm}")
			.check(css("a.thumb", "href").saveAs("pdpUrl"))
			.headers(Headers.searchPageHeaders_1))
		


}


